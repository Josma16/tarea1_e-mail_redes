# -*- coding: utf-8 -*-
from __future__ import print_function

import sys
import csv, smtplib, ssl

from email.mime.text import MIMEText

from twisted.python import log
from twisted.mail.smtp import sendmail
from twisted.internet import reactor

"""
Funcion que envia el mensaje al buzon local del dominio dado
Entrada: message: mensaje del emisor, subject: asunto del correo, sender: emisor, recipients: destinatarios del correo
(formato lista separado por ','), host: dominio del server a enviar el correo, p: numero de puerto 
"""
def send(message, subject, sender, recipients, host, p):
    """
    Send email to one or more addresses.
    """
    msg = MIMEText(message)
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = ', '.join(recipients)

    dfr = sendmail(host, sender, recipients, msg.as_string(), port=p)
    dfr.addBoth(lambda result: reactor.stop())
    reactor.run()

"""
Funcion que envia el mensaje a destinatarios de gmail
Entrada: message: mensaje del emisor, subject: asunto del correo, sender: emisor, recipients: destinatarios del correo
(formato lista separado por ','), host: dominio del server a enviar el correo 
"""
def sendgmail(message, subject, sender, recipients, host):
    msg = MIMEText(message)
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = ', '.join(recipients)
    password = input('Ingrese la contraseña: ')
    context = ssl.create_default_context() #Aqui se genera un cifrado ssl
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(sender, password)
        server.sendmail(sender, recipients, msg.as_string())

"""
Divide el archivo correo en una lista de sus lineas para trabajar la construccion del mensaje
Entrada: message: archivo de mensaje
Salida: lista de strings con las lineas del archivo 
"""
def divide_message(message):
    f = open(message, "r")
    lines = f.readlines()
    return lines

"""
Unifica el mensaje que envia el emisor 
Entrada: message: lista con strings que forman el mensaje
Salida: string con el mensaje unificado 
"""
def merge_body(body):
    new_body = ""
    for b in body:
        new_body = new_body + b
    return new_body

"""
Toma cada correo de un csv y los inserta en una lista
Entrada: ruta del archivo csv a leer
Salida: lista de strings con cada destinatario 
"""
def read_destinies(newcsv):
    recipients = []

    with open(newcsv) as File:
        reader = csv.reader(File)
        for row in reader:
            recipients.append(row[0])
    return recipients


if __name__ == '__main__':

    import argparse
    
    parser = argparse.ArgumentParser(description='Interfaz de envío de mensajes.')
    parser.add_argument('-d', action='store',
                    dest='servidor_correo',
                    help='Nombre del servidor')

    parser.add_argument('-c', action='store',
                    dest='archivo_csv',
                    help='Archivo csv con los destinatarios')

    parser.add_argument('-m', action='store',
                    dest='mensaje',
                    help='Mensaje a enviar')

    parser.add_argument('-p', action='store',
                        dest='puerto',
                        help='Puerto a enviar')

    args = parser.parse_args()

    message_parts = divide_message(args.mensaje)
    body = merge_body(message_parts[2:])

    recipients = read_destinies(args.archivo_csv)

    log.startLogging(sys.stdout)

    if args.servidor_correo == "smtp.gmail.com":
        sendgmail(body, message_parts[1], message_parts[0][:-1], recipients, args.servidor_correo)
    else:
        send(body, message_parts[1], message_parts[0][:-1], recipients, args.servidor_correo, int(args.puerto))