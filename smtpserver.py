# -*- coding: utf-8 -*-
import sys
import os

from email.header import Header
from zope.interface import implementer
from twisted.internet import defer, reactor, ssl
from twisted.mail import smtp, maildir
from twisted.python import log
from twisted.internet.protocol import Factory, Protocol

"""
Valida la llegada de mensajes y envía un mensaje en el servidor en modo de aviso, hay validaciones de dominio
"""
class LocalMessageDelivery(object):
    
    @implementer(smtp.IMessageDelivery)
    
    def __init__(self, protocol, baseDir, hostname):
        self.protocol = protocol
        self.baseDir = baseDir
        self.hostname = hostname
    """
    Chequea el header del mensaje para ver si cumple con los estándares del protocolo. 
    """
    def receivedHeader(self, helo, origin, recipients):
        clientHostname, clientIP = helo
        myHostname = self.protocol.transport.getHost().host
        headerValue = "from %s by %s with ESMTP ; %s" % (
        clientHostname, myHostname, smtp.rfc822date())
        return "Received: %s" % Header(headerValue)

    """
    Valida el origen del mensaje    
    """
    def validateFrom(self, helo, origin):
        # Accept any sender.
        return origin

    """
    Toma la dirección del buzón
    """
    def _getAddressDir(self, address):
        return os.path.join(self.baseDir, "%s" % address)

    """
    Valida que el dominio de los destinatarios son iguales al dominio del servidor
    """
    def validateTo(self, user):

        if user.dest.domain == self.hostname:
            return lambda: MaildirMessage(
                    self._getAddressDir(str(user.dest)))
        else:
            log.msg("Received email for invalid recipient %s" % user)
            raise smtp.SMTPBadRcpt(user)

"""
Acondiciona la dirección física dada para tratarlo como un buzón de correos)
"""
class MaildirMessage(object):
    
    @implementer(smtp.IMessage)
    
    def __init__(self, userDir):
        if not os.path.exists(userDir):
            os.mkdir(userDir)
        inboxDir = os.path.join(userDir, 'Inbox')
        self.mailbox = maildir.MaildirMailbox(inboxDir)
        self.lines = []
        
    def lineReceived(self, line):
        self.lines.append(line)
        
    def eomReceived(self):
        print ("New message received.")
        self.lines.append('') # Add a trailing newline.
        messageData = '\n'.join(self.lines)
        return self.mailbox.appendMessage(messageData)

    def connectionLost(self):
        print ("Connection lost unexpectedly!")
        del self.lines

"""
 Inicializa el protocolo SMTP 
"""
class LocalSMTPFactory(smtp.SMTPFactory):
    
    def __init__(self, baseDir, hostname):
        self.baseDir = baseDir
        self.hostname = hostname
    
    def buildProtocol(self, addr):
        proto = smtp.ESMTP()
        proto.delivery = LocalMessageDelivery(proto, self.baseDir, self.hostname)
        return proto

"""
Inicio de la llamada. Toma todas las opciones del .py y las mete en el args. Llama a LocalSMTPFactory para levantar
el servidor
"""
if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Interfaz de envío de mensajes.')
    parser.add_argument('-d', action='store',
                        dest='servidor_correo',
                        help='Nombre del servidor')

    parser.add_argument('-s', action='store',
                        dest='buzon',
                        help='Ubicacion a guardar archivos')

    parser.add_argument('-p', action='store',
                        dest='puerto',
                        help='Puerto del servidor')
    args = parser.parse_args()
    
    hostname = args.servidor_correo
    log.startLogging(sys.stdout)

    reactor.listenTCP(int(args.puerto), LocalSMTPFactory(args.buzon, hostname))

    reactor.run()
